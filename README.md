# Ansible Site Example Job

This is an Ansible job to run and execute tasks when needed. Scheduling and additional settings can be controlled here, while the playbook itself is part of the full site.

See the base [ansible-site-example](https://gitlab.com/lazyfrosch-org/ansible-site-example)

## License

Licensed under [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
